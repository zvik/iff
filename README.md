# IFF
IFF - **Identification friend or foe**, for the ground force.

you can read on [wikipedia](https://en.wikipedia.org/wiki/Identification_friend_or_foe) about the history IFF in the air fource.

## setup:
1. clone git repo `git clone https://gitlab.com/zvik/iff`.
2. to make sure you have python3 installed, type in your terminal `python --version`.
3. install packages:
   - crypto - package with encryption algorithms. run `pip install pycryptodome` on windows, on linux run `pip install pycrypto`.
   - matplotlib - for displaying 2d graphics. run `pip install matplotlib` on windows or linux.
4. cd to the project root.
5. to run the sever type `python iff-server` in your terminal.
6. to run a device type `python iff-device` in another terminal window.

## some notes:
* please 🙏 use meanfull names for variabels and functions.
* add comments of what you did and add extra explenation if nedded.
* commect evey function that is public.
* keep private functions private 🤓.
* keep your code simple as possible.
* if you want to create a test file for local testing, name it `is-test-*.py` and git would ignore it.

## inspiration:
🚀
💣
💥
🍌
🔪
🔫