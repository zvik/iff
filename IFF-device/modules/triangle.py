class Triangle:

    def __init__(self, p0, p1, p2):
        self.p0 = p0
        self.p1 = p1
        self.p2 = p2

    def p_in_triangle(self, p):
        dX = p.x-self.p2.x
        dY = p.y-self.p2.y
        dX21 = self.p2.x-self.p1.x
        dY12 = self.p1.y-self.p2.y
        D = dY12*(self.p0.x-self.p2.x) + dX21*(self.p0.y-self.p2.y)
        s = dY12*dX + dX21*dY
        t = (self.p2.y-self.p0.y)*dX + (self.p0.x-self.p2.x)*dY
        if (D < 0):
            return (s <= 0 and t <= 0 and s+t >= D)
        return (s >= 0 and t >= 0 and s+t <= D)

    def copy(self):
        return Triangle(self.p0, self.p1, self.p2)

    def print(self):
        print("Triangle (" + str(self.p0) + "," +
              str(self.p1) + "," + str(self.p2) + ")")

    def to_string(self):
        return("(" + str(self.p0) + "," + str(self.p1) + "," + str(self.p2) + ")")
