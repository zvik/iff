class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def copy(self):
        return Point(self.x, self.y)
    
    def print(self):
        print("Point (" + str(self.x) + "," + str(self.y) + ")")

    def to_string(self):
        return("(" + str(self.x) + "," + str(self.y) + ")")