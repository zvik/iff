import lib.communication as communication
import lib.encryption as encryption

DIR = "> src/server_connect/"
key = ""


def connect_to_server():
    """ tries to connects to server. """
    connected = communication.join_server()
    return connected


def authorize_device(id):
    """
        - get the userId for auth.
        - autherizes itself and receivs a AES key from the server.
        - return the AES key if autherixzes, else returns False.
    """
    global key
    if (not autherizes_with_id(id)):
        print("failed at step 1")
        return False
    if (not approved_id()):
        print("failed at step 2")
        return False
    if (not get_aes_key()):
        print("failed at step 3")
        return False
    return {"key": key}


def autherizes_with_id(id):
    """ part 1 of the auth proccess, sends to the server the usersId. """
    msg = communication.receive()
    if (msg == "[a]get_id"):
        return communication.send("[r]" + str(id))
    else:
        return False


def approved_id():
    """ part 2 of the auth proccess, makes sure the server exepted the userId. """
    msg = communication.receive()
    return msg == "[i]connected"


def get_aes_key():
    """
        - part 3 of the auth proccess, recives AES key from the server via RAS.
        - the device creates a RAS connection for transferingn the AES key.
    """
    global key
    key_map = encryption.generate_rsa_key()
    communication.send("[a]rsa_public_key:"+key_map["public"].decode())
    aes_encrypted_key = communication.receive(False)
    key = aes_encrypted_key
    key = encryption.decrypt_rsa(aes_encrypted_key, key_map["private"])
    a = communication.send("[i]key_recived")
    msg = communication.receive()
    return msg == "[i]disconnect_rsa"


def start_running():
    communication.send("[a]ready")
    responce = communication.receive()
    return responce == "[r]start"