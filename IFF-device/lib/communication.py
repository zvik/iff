from socket import AF_INET, socket, SOCK_STREAM


HOST = '127.0.0.1'
PORT = 33001
BUFSIZ = 1024
ADDR = (HOST, PORT)
CLIENT = socket(AF_INET, SOCK_STREAM)


# this librery handles the comunication between the server and the client


def join_server():
    """
        - joins the server, return true.
        - if can't join (e.g. server offline), return false.
    """
    try:
        CLIENT.connect(ADDR)
        return True
    except:
        return False


def send(msg, encode=True):
    """
        - sends a message to the server, return true.
        - if can't send (e.g. server offline), return false.
        - also is set to encode the message by default for sending.
    """
    # try:
    if (encode):
        msg = bytes(msg, "utf8")
    CLIENT.send(msg)
    return True
        # return True
    # except:
        # return False


def receive(decode=True):
    """
        - waits until recives a message from the server.
        - return the message, decoded.
        - if can't wait for reciveing message (e.g. server offline), return false.
    """
    # try:
    msg = CLIENT.recv(BUFSIZ)
    if (decode): msg = msg.decode("utf8")
    return msg
    # except:
        # return False
