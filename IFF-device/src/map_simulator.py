import turtle


# the class simulate the devices map.


WIDTH = 600 # [10+]
HEIGHT = 600 # [10+]
ANGLE = 60 # [0 - 180]
DISTENCE = 100 # [0+]
devices_in_map = {}
wn = ""


def build_map():
    """ builds a map with the user in the middle. """
    global wn
    wn = turtle.Screen()
    wn.title("IFF map")
    wn.bgcolor("green")
    wn.setup(width=WIDTH, height=HEIGHT)
    wn.tracer(0)
    user_device = turtle.Turtle()
    user_device.speed(0)
    user_device.color("blue")
    user_device.shape("circle")
    user_device.penup()


def update_map(location, devices, in_danger):
    """
        - updates the location of the other devices on the map.
        - only run after running 'build_map()'.
    """
    global wn
    global devices_in_map
    compass = turtle.Turtle()
    if (in_danger):
        compass.color("red")
        compass.width(5)
    compass.left(location["compass"])
    compass.forward(100)
    compass.left(120)
    compass.forward(100)
    compass.left(120)
    compass.forward(100)
    compass.goto(0, 0)
    compass.penup()
    compass.hideturtle()
    norm_x = location["gps"]["lat"]
    norm_y = location["gps"]["lon"]
    for id in devices:
        if id not in devices_in_map:
            devices_in_map[id] = [turtle.Turtle(), turtle.Turtle()]
            devices_in_map[id][0].speed(0)
            devices_in_map[id][0].shape("circle")
            devices_in_map[id][0].color("red")
            devices_in_map[id][0].penup()
            devices_in_map[id][1].speed(0)
            devices_in_map[id][1].shape("circle")
            devices_in_map[id][1].color("blue")
            devices_in_map[id][1].turtlesize(0.2)
            devices_in_map[id][1].penup()
        x = (devices[id]["lat"] - norm_x) * 100000
        y = (devices[id]["lon"] - norm_y) * 100000
        devices_in_map[id][0].goto(x, y)
        devices_in_map[id][1].goto(x, y)
    wn.update()
    compass.reset()
    compass.hideturtle()
