import machine
import network
import utime
import time
import os
import curl
import telnt_web
import random
import math

print("\n\n\n\n")
print("Starting WiFi ...")
sta_if = network.WLAN(network.STA_IF)
sta_if.active(True)
sta_if.connect("nissim-hot", "0544500635")
tmo = 50
while not sta_if.isconnected():
    utime.sleep_ms(100)
    tmo -= 1
    if tmo == 0:
        sta_if.disconnect()
        break

print("test11")

if tmo > 0:
    print("WiFi started")
    print("\n =====  TelnT  08-11-2017 FTP as WIFI settings 1 ====\n")
    print(sta_if.ifconfig())
    print()
    utime.sleep_ms(500)
    rtc = machine.RTC()
    print("Synchronize time from NTP server ...")
    rtc.ntp_sync(server="hr.pool.ntp.org")
    tmo = 100
    while not rtc.synced():
        utime.sleep_ms(100)
        tmo -= 1
        if tmo == 0:
            break
    if tmo > 0:
        print("Time set")
        utime.sleep_ms(500)
        t = rtc.now()
        utime.strftime("%c")
        print("time:", t)

ll = os.listdir()

files = []
files = os.listdir()


x1 = 112
y1 = 213
z1 = 89
location1 = [x1, y1, z1]
direction1 = 100
fire1 = False

x2 = 88
y2 = 150
z2 = 200
location2 = [x2, y2, z2]
direction2 = 90
fire2 = False

x3 = 250
y3 = 300
z3 = 310
location3 = [x3, y3, z3]
direction3 = 180
fire3 = False

alert = False
whoFires = ''
onWho = ''

combinedGo = False


def gpsEmulator(x, y ,z, t1, t2, t3) : #emulate gps locations
    time.sleep(1)
    for i in range (0, t1) :
        x += 1
    for i in range (0, t2) :
        y += 1
    for i in range (0, t3) :
        z += 1
    return [x, y, z]

def directionEmulator(d, t) :
    for i in range (0, t) :
        if (t % 2 == 0 & d < 320) :
            d += 1
        elif (t % 2 == 1 & d > 15) :
            d -= 1
    return d

def fire() :
    global fire1, fire2, fire3
    n = random.randint(0, 20)
    for i in range(1, 30):
        if n % i == 0 :
            if n % 3 == 1 :
                fire1 = True
            elif n % 3 == 2 :
                fire2 = True
            elif n % 3 == 0 :
                fire3 =True
            elif n % 4 == 0 :
                fire1, fire2, fire3 = False
        print("Group 1 fire = " + str(fire1) + " Group 2 fire = " + str(fire2) + "Group 3 fire = " + str(fire3))


#gps1 is random for testing
def gps1() :
    global location1
    n = random.randint(0, 20)
    print("gps1 = " + str(location1))
    for i in range (0, 30) :
        location1 = gpsEmulator(location1[0], location1[1], location1[2], n+i, 10+n, 0)
        print("After 3 sec: gps1moved = " + str(location1))

def gps2() :
    global location2
    print("gps2 = " + str(location2))
    for i in range (0, 30) :
        location2 = gpsEmulator(location2[0], location2[1], location2[2], 0, 3+i, 0)
        print("After 3 sec: gps2 moved = " + str(location2))

def gps3() :
    global location3
    print("gps3 = " + str(location3))
    for i in range (0, 30) :
        location3 = gpsEmulator(location3[0], location3[1], location3[2], 12+i, 30, 0+i)
        print("After 3 sec: gps3 moved = " + str(location3))


#compass1 is random for testing
def compass1() :
    n = random.randint(0, 20)
    global direction1
    if (combinedGo == False) :
        for i in range (0, 30) :
            time.sleep(1)
            if ((i - n) % 4 == 0) :
                direction1 = directionEmulator(direction1, i)
            print("Direction COMPASS (0-360) : Direction1 = " + str(direction1))
    if (combinedGo == True) :
        if (n % 4 == 0) :
                direction1 = directionEmulator(direction1, n)

def compass2() :
    n = random.randint(0, 20)
    global direction2
    if (combinedGo == False) :
        for i in range (0, 30) :
            time.sleep(1)
            direction2 = directionEmulator(direction2, i - 10)
            print("Direction COMPASS (0-360) : Direction2 = " + str(direction2))
    if (combinedGo == True) :
        direction2 = directionEmulator(direction2, n)
        
def compass3() :
    n = random.randint(0, 20)
    global direction3
    if (combinedGo == False) :
        for i in range (0, 30) :
            time.sleep(1)
            if (direction3 < 303 & direction3 > 60) :
                direction3 = directionEmulator(direction3, i - 10)
            print("Direction COMPASS (0-360) : Direction3 = " + str(direction3))
    if (combinedGo == True) :
        direction3 = directionEmulator(direction3, n)

def calculate(location1, location2, location3, direction1, direction2, direction3, fire1, fire2, fire3) :
    global whoFires, onWho, alert
    alerted1 = False
    alerted2 = False
    alerted3 = False
    distance1 = math.sqrt((location1[0]-location2[0])**2 + (location1[1]-location2[1])**2) #from 1 to 2
    distance2 = math.sqrt((location2[0]-location3[0])**2 + (location2[1]-location3[1])**2) #from 2 to 3
    distance3 = math.sqrt((location3[0]-location1[0])**2 + (location3[1]-location1[1])**2) #from 3 to 1
    #print(str(distance1) + " | " + str(distance2) + " | " + str(distance3))
    if (fire1 == True) :
        impactLocation1 = [distance1*math.tan(direction1), distance1*math.tan(direction1)]
        if ((impactLocation1[0] - location2[0]) < distance1*math.sin(5) and (impactLocation1[1] - location2[1]) < distance1*math.sin(5)) :
            alert = True
            whoFires = "Group 1"
            onWho = "Group 2"
            alerted1 = True
        if ((impactLocation1[0] - location3[0]) < distance1*math.sin(5) and (impactLocation1[1] - location3[1]) < distance1*math.sin(5)) :
            alert = True
            whoFires = "Group 1"
            onWho = "Group 3"
            alerted1 = True
   
    if (fire2 == True) :
        impactLocation2 = [distance2*math.tan(direction2), distance2*math.tan(direction2)]
        if ((impactLocation2[0] - location1[0]) < distance2*math.sin(5) and (impactLocation2[1] - location1[1]) < distance2*math.sin(5)) :
            alert = True
            whoFires = "Group 2"
            onWho = "Group 1"
            alerted2 = True
        if ((impactLocation2[0] - location3[0]) < distance2*math.sin(5) and (impactLocation2[1] - location3[1]) < distance2*math.sin(5)) :
            alert = True
            whoFires = "Group 2"
            onWho = "Group 3"
            alerted2 = True
        
    if (fire3 == True) :
        impactLocation3 = [distance3*math.tan(direction3), distance3*math.tan(direction3)]
        if ((impactLocation3[0] - location1[0]) < distance3*math.sin(5) and (impactLocation3[1] - location1[1]) < distance3*math.sin(5)) :
            alert = True
            whoFires = "Group 1"
            onWho = "Group 2"
            alerted3 = True
        if ((impactLocation3[0] - location2[0]) < distance3*math.sin(5) and (impactLocation3[1] - location2[1]) < distance3*math.sin(5)) :
            alert = True
            whoFires = "Group 1"
            onWho = "Group 3"
            alerted3 = True
    
    if (alerted1 == False & alerted2 == False & alerted3 == False) :
        alert = False
        whoFires = ''
        onWho = ''
        
def alertTest() :
    global location1, location2, location3, fire1, fire2, fire3
    fire1 = True
    fire3 = True
    fire2 = True
    location1 = [0, 0, 0]
    location2 = [0, 0, 0]
    location3 = [0, 0, 0]
    combined()
    
def combined() :
    global location1, location2, location3, direction1, direction2, direction3, fire1, fire2, fire3, combinedGo
    combinedGo = True
    n = random.randint(0, 20)
    print("START: \ngps1 = " + str(location1) + " gps2 = " + str(location2) + " gps3 = " + str(location3))
    print("Direction1 = " + str(direction1) + " | Direction2 = " + str(direction2) + " | Direction3 = " + str(direction3))
    for i in range (1, 30) :
        if (direction3 > 340) :
            direction3 = 5
        if n % i == 0 :
            if n % 3 == 1 :
                fire1 = True
            elif n % 3 == 2 :
                fire2 = True
            elif n % 3 == 0 :
                fire3 = True
        location1 = gpsEmulator(location1[0], location1[1], location1[2], n+i, 10+n, 0)
        location2 = gpsEmulator(location2[0], location2[1], location2[2], 0, 3+i, 0)
        location3 = gpsEmulator(location3[0], location3[1], location3[2], 12+i, 30, 0+i)
        #direction1 = directionEmulator(direction1, i - n)
        #direction2 = directionEmulator(direction2, i - 10)
        #direction3 = directionEmulator(direction3, i + 10)
        compass1()
        compass2()
        compass3()
        calculate(location1, location2, location3, direction1, direction2, direction3, fire1, fire2, fire3)
        print("-------\nAfter 3 sec: \ngps1moved = " + str(location1) + " \ngps2moved = " + str(location2) + " \ngps3moved = " + str(location3))
        print("\nDirection COMPASS (0-360) \nDirection1 = " + str(direction1) + " \nDirection2 = " + str(direction2) + " \nDirection3 = " + str(direction3))
        print("\nGroup 1 fire = " + str(fire1) + " \nGroup 2 fire = " + str(fire2) + "\nGroup 3 fire = " + str(fire3))
        print("alert = " + str(alert))
        if (alert == True) :
            print("\nALERT ALERT ALERT " + whoFires + " is firing on " + onWho +"\n")

def l1():
    my_dir = os.getcwd()
    print('do(6)\nos.getcwd=', my_dir)
    # print(os.listdir())    
    # files=os.listdir()
    for i in range(len(files)):
        print(i, files[i])


def g1():  # How to import GPS cordinates.
    print("g1 started")
    return [0, 0, 0]


def g2():
    print("g2 started")
    return [[100, 200, 100], [500, 600, 900], [100, 0, 10],[1000, 2000, 1000]]


def fireold2():
    print("Fire array started")
    return [[1000, 2000, 1000], [500, 600, 900]]


def start():
    self = g1()  # Has to be RETURN in g1 [x,y,z] /meters\
    others = g2()  # Has to be RETURNed as an array of locations [[x1,y1,z1],[x2,y2,z2]..].
    firearray = fire()  # Has to RETURN an array or a list of those who are firing [[x'1,y'1,z'1],[x'2,y'2,z'2]..].
    # Max distance for testing is 300m.
    k = 0
    for i in range(len(others)):
        for j in range(len(firearray)):
            if others[i] == firearray[j]:
                if abs(self[0] - others[i][0]) < 300 or abs(self[1] - others[i][1]) < 300 or abs(
                        self[2] - others[i][2]) < 300:
                    k += 1

    if k > 0:
        print("ALERT! ALERT! ALERT!")


def cd(to_dir):
    os.chdir(to_dir)
    l1()


def type(filename):
    with open(filename, 'rt') as f:
        for line in f.readlines():
            # print(line, end == '')
            print(line)
            if line == "":
                filename.closed
                print("\n\n\n")


def ip():
    print(sta_if.ifconfig())


def exe(f):
    exec(open(f).read())


def reset():
    machine.reset()


def do(i):
    exec(open(files[i]).read())


ll
print("\n ll = os.listdir()")

if (network.ftp.start() == 1):
    print("\n===== FTP is ON ======\n", sta_if.ifconfig())
else:
    print("\n===== FTP is OFF =====\n")

# print(curl.get("http://google.com"))
# print(curl.get("http://TelnT.com"))
# print('============================ \
#  =============================')
print("see all curl command at https: \
  //github.com/loboris/ESP32_curl_example\n\n")

l1()
ip()
print("l1() ip() do() reset() exe() type() cd() telnet user=' ' ,password=''\n\n")
print("Welcome to iProtector,/n This program will help you to prevent friendly fire.\n")
print("What would you like to do?\n")
print("To check and update one of the 3 GPS locations type 'gps1()', 'gps2()', 'gps3()' .\n")
print("To check the 3 compass type 'compass1()', 'compass2()', 'compass3()' .\n")
print("To see who is firing type 'fire()' .\n")
print("To see your full information combined type 'combined()' .\n")
print("To get a high probability of friendly fire type 'alertTest()' .\n")
print("To exit the system press 'esc' .\n")
