# import tkMessageBox
import datetime
import matplotlib.pyplot as plt # libary to draw the graphs.
import os #to get current directory.
import re # used to make regular expressions.
#--------------------------
#plt.plot(l1)
#plt.plot(l2)
#plt.show
# => generate multiple lines.
#--------------------------

indicator = 2600 # the highest this value is the less sensitive it is.
gap = 5 # number of frames to evaluate for pick (sound pick)

RECORD_DIC = {
    "silence" : "06092018_2050",
    "meavrer" : "06092018_2053",
    "next_to_pc" : "06092018_2056",
    "hand_clup_a" : "06092018_2100",
    "silent_guitar" : "06092018_2105",
    "hard_rock" : "06092018_2108",
    "bloon_pop" : "12092018_2155",
    "hand_clups" : "21092018_1510",
    "hand_clup_b" : "21092018_1515",
    "hand_clup_c" : "21092018_1517",
    "hit_a_pan_with_spoon" : "24102018_2200",
    "beside_laptop" : "24102018_2232",
    "silence_and_scream" : "24102018_2238",
    "water_drinking" : "21092018_1520"
}

TEXT_NAME = RECORD_DIC.get("hand_clup_b") # here we enter the name of the file we want to work on.

#ALPHA = 0.99 # here we set an alpha varriable.

def start():# sample method for using plt libary.
    #here are our samples:
    samples = [100,92,95,80,70,60,85,95,104,109,100,99,97,90,87]
    plt.plot(samples)
    plt.show()

def extract_avg(): # this function draws the graph.
    file = open(os.path.dirname(__file__) + "/" + TEXT_NAME + ".txt", 'r')# os.path.dirname(__file__) - is the location of this current folder.
    l1 = []
    for line in file:
        l1.append(find_and_print_avg(line)) # here we generate list of avarages.
    # plt.plot(l1)
    # plt.show()
    l_origin = l1
    l_99 = generate_list_with_alpha(l_origin ,0.99 ) # here we generate list of avarage using alpha = 0.99 this time.
    l_95 = generate_list_with_alpha(l_origin, 0.95)  # here we generate list of avarage using alpha = 0.95 this time.
    l_90 = generate_list_with_alpha(l_origin, 0.9)  # here we generate list of avarage using alpha = 0.9 this time.
    l_80 = generate_list_with_alpha(l_origin, 0.8)  # here we generate list of avarage using alpha = 0.8 this time.
    plt.plot(l_origin , linewidth=0.8)
    #plt.plot(l_99 , linewidth=0.8)
    plt.plot(l_95 , linewidth=1.5)    #plt.plot(l_90, linewidth=0.8)
    plt.plot(l_80, linewidth=0.8)
    plt.legend(['original', 'alpha = 0.95', 'alpha = 0.8'], loc='upper left')
    #plt.legend(['original', 'alpha = 0.99' , 'alpha = 0.95' ,'alpha = 0.9' ,'alpha = 0.8' ], loc='upper left')
    print ("original points: ")
    print (l_origin)
    print ("alpha 95 : ")
    print (l_95)

    #send to indicator for evaluation:
    if checkList(l_95, gap) > indicator:
        # tkMessageBox.showinfo(title="ALERT", message="ALERT IS ON !")
        print ("ALERT IS ON")
        return True
    else:
        # tkMessageBox.showinfo(title="sucsses", message="no alert today :)")
        print ("alert off")
        return False

    # show graph:
    plt.show()

def generate_list_with_alpha(l1 , alphaVar):
    l2 = []
    for i in range(len(l1)):
        l2.append(l1[i] * (1-alphaVar) + alphaVar * (sum(l1[0:i+1]))/(i+1)) #core of the code - here we use alpha var.
    return l2

def find_and_print_avg(line): # extract the avg from the text file, and return it as a float number.
    m = re.search('squares average = [0-9]+\.[0-9]+', line)
    str2 = m.group()
    str2 = str2.replace(" ", "")
    str2 = str2.replace("squaresaverage=", "")
    return float(str2)

#-------------indicating an alert : -----------------------
def checkList(l1 , gap ): # return the max Gap between the list.
    maxGap = 0
    for i in range (50 , len(l1) - gap) :
        maxGap = getMaxGap(l1[i:i + gap]) if getMaxGap(l1[i:i + gap]) > maxGap else maxGap
    return maxGap

def getMaxGap(l1):
    return max(l1) - min(l1)
#----------------------------------------------------------

if __name__ == "__main__":
    #start()
    extract_avg()
    # tkMessageBox.showinfo(title="Current Time", message="The time is : " + str(datetime.datetime.now()))




