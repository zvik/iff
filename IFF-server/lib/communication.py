from socket import AF_INET, socket, SOCK_STREAM
import asyncio

DIR = "> lib/communication/"
HOST = ''
PORT = 33001
BUFSIZ = 1024
ADDR = (HOST, PORT)

SERVER = socket(AF_INET, SOCK_STREAM)
SERVER.bind(ADDR)


def create_server():
    SERVER.listen(5)
    print(DIR + "create_server: server successfuly created.")
    return True


def await_new_clients():
    while True:
        client, client_address = SERVER.accept()
        print(DIR + "await_new_devices: " +
              str(client_address) + " has connected.")
        client_detailes = {"client": client, "address": client_address}
        yield client_detailes


async def disconnect_client(client):
    await client.close()
    return True


def send(client, msg, encode=True):
    if (encode):
        msg = bytes(msg, "utf8")
    client.send(msg)
    # print(DIR + "send: " + str(msg) + " was sent.")
    return True


def receive(client):
    msg = client.recv(BUFSIZ)
    msg = msg.decode("utf8")
    # print(DIR + "receive: " + str(msg) + " was recived.")
    return msg
