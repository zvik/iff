# this script is using the alsaaudio package to record audio, using a usb-microphone connected to a RPi0w board

from __future__ import print_function
from os import getenv
import sys
import time
import getopt
import alsaaudio
import copy


def usage():
    print('usage: recordtest.py [-d <device>] <file>', file=sys.stderr)
    sys.exit(2)


def frame_average(frame_len_t,frame_data_t):
    # calculate the average value of the current frame
    frame_average=0
    for n in range(frame_len_t):
        # translate to integer
        frame_data_t_n=frame_data_t[n]
        # accumulate the values
        frame_average=frame_average+frame_data_t_n
        
    # divide by the number of samples
    frame_average=frame_average/frame_len_t
    return frame_average


def frame_sqr_average(frame_len_t,frame_data_t):
    # calculate the average value of the current frame
    frame_sq_average=0
    for n in range(frame_len_t):
        # translate to integer
        frame_data_t_n=frame_data_t[n]
        frame_data_sq_t_n=frame_data_t_n*frame_data_t_n
        # accumulate the values
        frame_sq_average=frame_sq_average+frame_data_sq_t_n
        
    # divide by the number of samples
    frame_sq_average=frame_sq_average/frame_len_t
    return frame_sq_average


if __name__ == '__main__':

    device = 'default'

    opts, args = getopt.getopt(sys.argv[1:], 'd:')
    for o, a in opts:
        if o == '-d':
            device = a

    if not args:
        usage()

    f = open(args[0],'wb') # b represent binary

    # Open the device in blocking mode. could
    # just as well have been non blocking mode.
    #Then we need to put sleep(0.001) in the end of the loop

    inp = alsaaudio.PCM(alsaaudio.PCM_CAPTURE, alsaaudio.PCM_NORMAL, device=device, cardindex=1)

    # set parameters
    fs = 44100 # sampling rate [Hz] - this is the only option for our hardware
    frame_len = 2205 # the length of a frame
    T = 10 # the length of the entire recording [seconds]
    N_frames = round(T*fs/frame_len) # the number of frames that we should record to get the desired length
    # inp.setrate(fs)
    inp.setperiodsize(frame_len)
    # the format of a sample - PCM_FORMAT_S16_LE is a 16 bit signed integer, with little-endian orientation
    inp.setformat(alsaaudio.PCM_FORMAT_S16_LE)
	# number of channels - only one in this script
    inp.setchannels(1)
	
    # fAllData = open("allData.txt" , 'a+')#avg
    # fAvg = open("avgFile.txt" , 'a+')#avg
    # fAvgSqr = open("avgSqrtFile.txt" , 'a+')#avg squared
    # sumOfFrame=0
    # sumSqr=0 # this is the sum for each data [i] - squared.
    
    print('begin recording...')
    print('sampling rate = ',fs,', frame length = ',frame_len,' [samples], recording length = ',T,' [seconds].')    
    
    for t_frames in range(N_frames):
        
	    # get a frame of data from the microphone
        frame_len_t, frame_data_t = inp.read() # l is an integer, data is a string
        
        # calculate the average of the samples in the current frame
        frame_avg_t=frame_average(frame_len_t,frame_data_t)

        # calculate the average of the squared samples in the current frame
        frame_avg_sq_t=frame_sqr_average(frame_len_t,frame_data_t)
        
        if frame_len_t: # as long as l !=0 (0 means false and when l=0 the data was lost)
            f.write(frame_data_t)
        
        # print some data to prompt
        print('frame #',t_frames,', length =',frame_len_t,'[samples], average =',frame_avg_t,', squares average =',frame_avg_sq_t)
