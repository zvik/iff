import base64
import os
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
from Crypto import Random
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import ast


# this librery handles the AES and RAS encryption and decryption.
#
# note that AES encryption rewquiers the message to be in a spesific
# length, therefor we add padding to the message when encrypting and
# remove it when decrypting.


def encrypt_aes(source, key, encode=True):
    """
        - encrypts a message with a given AES key.
        - return the encrypted message.
        - also is set to encode the encrypted message by default
          to prepare it for sending.
    """
    if encode:
        source = source.encode("utf8")
    key = SHA256.new(key.encode("utf8")).digest()
    IV = Random.new().read(AES.block_size)
    encryptor = AES.new(key, AES.MODE_CBC, IV)
    padding = AES.block_size - len(source) % AES.block_size
    source += bytes([padding]) * padding
    data = IV + encryptor.encrypt(source)
    return base64.b64encode(data) if encode else data


def decrypt_aes(source, key, decode=True):
    """
        - decodes a encrypted message with a given AES key.
        - return the message as plain text.
        - also is set to decode the encrypted message by default
          as if it has just been recived.
    """
    if decode:
        source = base64.b64decode(bytes(source, "utf8"))
    key = SHA256.new(key.encode("utf8")).digest()
    IV = source[:AES.block_size]
    decryptor = AES.new(key, AES.MODE_CBC, IV)
    data = decryptor.decrypt(source[AES.block_size:])
    padding = data[-1]
    if data[-padding:] != bytes([padding]) * padding:
        raise ValueError("Invalid padding...")
    return data[:-padding]


def encrypt_rsa(source, key):
    """
        - encrypts a message with a given RSA public key.
        - return the encrypted message.
        - also is set to encode the encrypted message by default
          to prepare it for sending.
    """
    source = source.encode("utf8")
    public_key = RSA.import_key(key)
    encryptor = PKCS1_OAEP.new(public_key)
    data = encryptor.encrypt(source)
    return data


def decrypt_rsa(source, key):
    """
        - decodes a encrypted message with a given RSA private key.
        - return the message as plain text.
        - also is set to decode the encrypted message by default
          as if it has just been recived.
    """
    private_key = RSA.import_key(key)
    decryptor = PKCS1_OAEP.new(private_key)
    decrypted = decryptor.decrypt(ast.literal_eval(str(source)))
    return decrypted


def generate_rsa_key():
    """
        - generates a RSA key.
        - returns the key as a dict with the private and public keys.
    """
    key = RSA.generate(1024)
    public = key.publickey().exportKey()
    private = key.exportKey()
    return {"private": private, "public": public}
