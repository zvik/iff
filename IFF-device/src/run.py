from threading import Thread
import asyncio
import ast
import turtle
import time
import math

import lib.encryption as encryption
import lib.communication as communication
import lib.compass as compass
import lib.gps as gps
import src.map_simulator as map_simulator
import src.comunication_setup as comunication_setup

from modules.point import Point
from modules.triangle import Triangle


# this is the main device class, the function [run] runs on device startup.


DIR = "> run/"
DIS = 100  # [0+]
ANG = 60  # [0 - 180]
MAP_REFRESH_SPEED = 0.95  # [0 - 1]
key = ""
id = "a"
map = {}
location = {"gps": {"lat": 0, "lon": 0}, "compass": 0}


# about the message types that are send to and from clients
# [a] 'action' it meens that there is a action that is requested from the client
# [r] 'responce' it can come only as a replay for a [a] (but is not required)
# [i] 'information' - no action is required from the client
# [l] 'location'


def run():
    """ retries to connect to the device until successfuly connected. """
    running = False
    while (running == False):
        running = startup()
        print(DIR + "run: error starting, retrying in 5 seconds.")
        time.sleep(5)

def startup():
    """ 
         - device startup function.
         - this function is devided to 5 step, each reallies on the previous.
    """
    global id
    global key

    # step 1: setup. ask the user to set for the device id, if not provided.
    if (id == ""):
        update_id = input("What is your id? ")
        id = str(update_id)

    # step 2: connection. connect to the server.
    connected = comunication_setup.connect_to_server()
    if (not connected):
        print(DIR + "startup: can't connect to server.")
        return False
    print(DIR + "startup: connected to server.")
	
    # step 3: autherization. give the server the devices id and recive a AES encrypting key.
    authed = comunication_setup.authorize_device(id)
    if (not authed):
        print(DIR + "startup: device not authed to connect.")
        return False
    key = authed["key"].decode()
    print(DIR + "startup: device authed.")

    # step 4: strtup. whait until the server tells the device to start sending location.
    start = comunication_setup.start_running()
    if (not start):
        print(DIR + "startup: server did't allow start.")
        return False
    print(DIR + "startup: device starting.")

    # step 5: run. use threads to run each part of the app simultaneously.
    RECEIVE_THREAD = Thread(target=receive)
    RECEIVE_THREAD.start()
    GPS_THREAD = Thread(target=listen_to_gps)
    GPS_THREAD.start()
    COMPASS_THREAD = Thread(target=listen_to_compass)
    COMPASS_THREAD.start()
    MAP_THREAD = Thread(target=display_map)
    MAP_THREAD.start()

def receive():
    """ 
         - awaits for incomming GPS location messages from the server.
         - when GPS location was recived, decrypet it and update the devices dict [map].
    """
    global map
    while True:
        encrypted_msg = communication.receive()
        msg = encryption.decrypt_aes(encrypted_msg, key)
        print(DIR + "receive: " + str(msg))
        msg = msg.decode("utf8")
        if (str(msg)[:3] == "[l]"):
            device = ast.literal_eval(msg[3:])
            map[device["id"]] = device["gps"]


def listen_to_gps():
    """
         - fairsup the GPS simulator.
         - when there is a GPS location update, update [location] and send it to the server.
    """
    global location
    gps.turn_on()
    gps_updates = gps.stream()
    for update in gps_updates:
        msg = "[l]" + str({"id": id, "gps": update})
        location["gps"] = update
        send(msg)


def listen_to_compass():
    """
         - fairsup the compass simulator.
         - when there is a compass degree update, update [location].
    """
    global location
    compass.turn_on()
    compass_updates = compass.stream()
    for update in compass_updates:
        location["compass"] = update


def display_map():
    """
         - simulates a map.
    """
    global map
    global location
    map_simulator.build_map()
    while True:
        static_location = location
        static_map = map
        danger = check_if_any_point_in_danger_zone(static_map, static_location)
        time.sleep(1 - MAP_REFRESH_SPEED)
        map_simulator.update_map(static_location, static_map, danger)


def send(msg):
    """
         - helper function.
         - sends a encrypted message to the server.
    """
    encrypted_msg = encryption.encrypt_aes(str(msg), key)
    communication.send(encrypted_msg, False)
    print(DIR + "send: " + str(msg))


def get_triangle_points(x1, y1, compass):
    """
         - helper function.
         - return a set of three point by a given starting point and compass.
         - also uses the ANG and DIS.
    """
    x2 = DIS*(math.cos(math.radians(compass)))
    y2 = DIS*(math.sin(math.radians(compass)))
    x3 = DIS*(math.cos(math.radians(compass+ANG)))
    y3 = DIS*(math.sin(math.radians(compass+ANG)))
    return (Point(x1, y1), Point(x2, y2), Point(x3, y3))


def check_if_any_point_in_danger_zone(map, location):
    """
         - helper function.
         - checks if anyone of the devices are in the danger zone.
         - returns True if there is one, else, return False.
    """
    points = get_triangle_points(0, 0, location["compass"])
    fire_zone = Triangle(points[0], points[1], points[2])
    for id in map:
        x = (map[id]["lat"] - location["gps"]["lat"]) * 100000
        y = (map[id]["lon"] - location["gps"]["lon"]) * 100000
        point = Point(x, y)
        if (fire_zone.p_in_triangle(point)):
            return True
    return False
