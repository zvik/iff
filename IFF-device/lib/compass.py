import random
import time


# this librery simulates a compass sensor.


on = False
FREQURNCY = 0.2
SPEED = 20


def turn_on():
    """ turns on the compass sensor """
    global on
    on = True


def turn_off():
    """ turns off the compass sensor """
    global on
    on = False


def stream():
    """
        - streams a random number as a thte compass degree.
        - if compass sensor is off, it well not stream.
        - the frequency of the stream is recived from [FREQURNCY].
        - the speed of the sensors movment is recived from [SPEED].
    """
    global on
    compass = 0
    while (on):
        time.sleep(FREQURNCY)
        compass += (random.random()-0.5) * SPEED
        yield compass
