from threading import Thread
import asyncio
import base64
import lib.encryption as encryption
import lib.communication as communication
import lib.audio as audio

import src.data as data


DIR = "> run/"
start = False
await_for_start = []

# about the message types that are send to and from clients
# [a] 'action' it meens that there is a action that is requested from the client
# [r] 'responce' it can come only as a replay for a [a] (but is not required)
# [i] 'information' - no action is required from the client
# [l] 'location'


def run():
    """ main function, creates a server and awaits for new devices on new threads. """
    communication.create_server()
    ACCEPT_THREAD = Thread(target=await_new_devices)
    ACCEPT_THREAD.start()
    AUDIO_THREAD = Thread(target=listen_to_mic)
    AUDIO_THREAD.start()


def listen_to_mic():
    """ awaits until the mic heres a gun shot. """
    global start
    can_start = audio.listen()
    if (can_start):
        start = can_start
        send_start_msg()


def await_new_devices():
    """ awaits for new devices to connect, when a device does, it opens a thread for it. """
    new_devices = communication.await_new_clients()
    for device in new_devices:
        try:
            Thread(target=connect_device, args=(device,)).start()
        except:
            print(DIR + "await_new_devices: closed device thread.")


def connect_device(device):
    """ after a device is connected, we check if it is a authurized device, only if it is we listen to its location. """
    authed = new_device(device["client"])
    if (authed):
        device_thread(device["client"])
    else:
        print(DIR + "connect_device: device not connected. destoring thread.")
        return False


def device_thread(client):
    """ lisening to the devices location (a thread) and call a function to brodcasts it to all other devices. """
    global start
    key = data.clients[client]["key"]
    while True:
        encripted_msg = communication.receive(client)
        try:
            msg = encryption.decrypt_aes(encripted_msg, key).decode("utf8")
        except:
            msg = encripted_msg
        # print(DIR + "device_thread: " + str(msg) + " was recived.")
        if (msg[:3] == "[l]"):
            broadcast(msg, client)
        elif (msg == "[a]ready"):
            if (start):
                communication.send(client, "[r]start")
                data.clients[client]['send'] = True
            else:
                await_for_start.append(client)
        elif (msg == "[a]disconnect"):
            communication.send(client, "[r]disconncting")
            client.close()
            break


def new_device(client):
    """ check if the new device it authed and passes it a aes key. """
    try:
        print(DIR + "new_device: adding new device " + str(client) + ".")
        communication.send(client, "[a]get_id")
        client_id = communication.receive(client)[3:]
        if (data.client_ids[client_id] == True):
            data.clients[client] = {"id": client_id, "send": False}
            data.client_ids[client_id] = False
            communication.send(client, "[i]connected")
            request = communication.receive(client)
            request_map = request.split(":")
            if (request_map[0] != "[a]rsa_public_key"):
                return False
            rsa_public_key = request_map[1].encode()
            aes_key = encryption.generate_aes_key()
            data.clients[client]["key"] = aes_key
            encrypted_aes_key = encryption.encrypt_rsa(aes_key, rsa_public_key)
            communication.send(client, encrypted_aes_key, False)
            request = communication.receive(client)
            if (request != "[i]key_recived"):
                return False
            communication.send(client, "[i]disconnect_rsa")
            return True
        else:
            print(DIR + "new_device: the given device id doesn't exist or is taken.")
            communication.send(client, "[i]connection_error")
            communication.disconnect_client(client)
            return False
    except:
        print(DIR + "new_device: unknown error connecting to new device.")
        return False


def broadcast(msg, client):
    """ encryepts the location and sends it to all connected devices """
    for sock in data.clients:
        if (client != sock):
            if (data.clients[sock] != None):
                try:
                    if (data.clients[sock]['send'] == True):
                    # print(DIR + "broadcast: " + str(msg) + " was sent to " + data.clients[sock]['id'] + ".")
                        encripted_msg = encryption.encrypt_aes(msg, data.clients[sock]['key'])
                        communication.send(sock, encripted_msg, False)
                except:
                    print(DIR + "broadcast: couldn't send message to " + data.clients[sock]['id'] + ".")

def send_start_msg():
    """ encryepts the location and sends it to all connected devices """
    for sock in await_for_start:
        try:
            communication.send(sock, "[r]start")
            data.clients[sock]['send'] = True
        except:
            print(DIR + "send_start_msg: couldn't send message to " + sock + ".")
