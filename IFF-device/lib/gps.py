import random
import time


# this librery simulates a GPS sensor.


on = False
FREQURNCY = 0.2
SPEED = 10


def turn_on():
    """ turns on the GPS sensor """
    global on
    on = True


def turn_off():
    """ turns off the GPS sensor """
    global on
    on = False


def stream():
    """
        - streams a random loctaion as a GPS location.
        - if GPS sensor is off, it well not stream.
        - the frequency of the stream is recived from [FREQURNCY].
        - the speed of the sensors movment is recived from [SPEED].
    """
    global on
    lat = random.random()/1000
    lon = random.random()/1000
    while (on):
        time.sleep(FREQURNCY)
        lat += (random.random()-0.5) / (SPEED * 500)
        lon += (random.random()-0.5) / (SPEED * 500)
        location = {"lat": lat, "lon": lon}
        yield location
